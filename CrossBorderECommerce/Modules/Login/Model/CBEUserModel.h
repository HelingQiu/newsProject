//
//  CBEUserModel.h
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/8/16.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBEUserModel : NSObject

@property(nonatomic, copy) NSString *userName;
@property(nonatomic, copy) NSString *password;
@property(nonatomic, copy) NSString *token;
@property(nonatomic, assign) BOOL isLogin;//是否登录

+ (instancetype)sharedInstance;

/**
 清空数据
 */
- (void)clearAllUserData;

@end
