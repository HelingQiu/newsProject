//
//  CBEUserModel.m
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/8/16.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEUserModel.h"

@implementation CBEUserModel

//创建单例
+ (instancetype)sharedInstance{
    
    static CBEUserModel * instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSUserDefaults *)defaults {
    
    return [NSUserDefaults standardUserDefaults];
}

- (void)setObject:(id)value forKey:(NSString *)key
{
    
    [self.defaults setObject:value forKey:key];
    [self.defaults synchronize];
}

- (void)setInteger:(NSInteger)value forKey:(NSString *)key
{
    
    [self.defaults setInteger:value forKey:key];
    [self.defaults synchronize];
}

- (void)setBool:(BOOL)value forKey:(NSString *)key
{
    
    [self.defaults setBool:value forKey:key];
    [self.defaults synchronize];
}

//取出数据
- (id)objectForKey:(NSString *)key {
    
    return [self.defaults objectForKey:key];
}

- (NSInteger)integerForKey:(NSString *)key
{
    
    return [self.defaults integerForKey:key];
}

- (BOOL)boolForKey:(NSString *)key
{
    
    return [self.defaults  boolForKey:key];
}

#pragma mark --
//是否已经登录
- (void)setIsLogin:(BOOL)isLogin
{
    [self setBool:isLogin forKey:@"isLogin"];
}
- (BOOL)isLogin
{
    return [self boolForKey:@"isLogin"];
}

- (void)clearAllUserData
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
}

@end
