//
//  CBELoginController.m
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/7/30.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBELoginController.h"
#import "CBEBaseTabBarController.h"
#import "CBEButton.h"

@interface CBELoginController ()

@property(nonatomic,strong)UIView * inputView;//用户名、密码输入白色背景
@property(nonatomic,strong)UIImageView * bgImageView;//背景图
@property(nonatomic,strong)CBEButton * btnLogin;//登录按钮

@end

@implementation CBELoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.bgImageView];
    [self.bgImageView addSubview:self.inputView];
    self.btnLogin.enabled = YES;
    
}

- (UIImageView *)bgImageView{
    
    if (!_bgImageView) {
        _bgImageView = [UIImageView new];
        _bgImageView.image = IMAGE_NAMED(@"背景");
        [self.view addSubview:_bgImageView];
        [_bgImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.bottom.mas_equalTo(self.view);
        }];
        _bgImageView.userInteractionEnabled = YES;
    }
    return _bgImageView;
}

-(UIView *)inputView{
    
    if (!_inputView) {
        _inputView = [UIView new];
        [self.bgImageView addSubview:_inputView];
        [_inputView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(kHalf(450.0f)/k6sHeight * kSCREEN_HEIGHT);
            CGFloat width = kSCREEN_WIDTH == 320.0f ? kSCREEN_WIDTH - 50.0f : kHalf(644.0f);
            make.width.mas_equalTo(width);
            make.height.mas_equalTo(kHalf(88.0f)*2 + kHalf(1.0f));
            make.centerX.mas_equalTo(self.view.mas_centerX);
        }];
        _inputView.layer.cornerRadius = 4;
        _inputView.backgroundColor = [UIColor whiteColor];
        
        UIView * sepLine = [UIView new];
        sepLine.backgroundColor = [UIColor lightGrayColor];
        [_inputView addSubview:sepLine];
        [sepLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(kHalf(88.0f));
            make.left.right.equalTo(self->_inputView);
            make.height.mas_equalTo(kHalf(1.0f));
        }];
    }
    return _inputView;
}

- (CBEButton *)btnLogin{
    
    if (!_btnLogin) {
        
        _btnLogin = [[CBEButton alloc] initWithFrame:CGRectZero title:@"登录" titleColor:[UIColor whiteColor] titleFontSize:16.0 backgroundColor:[UIColor blueColor] didClicked:^{
            [self.view endEditing:YES];
            [self presentHomePage];
        }];
        _btnLogin.layer.cornerRadius = 4;
        [_btnLogin setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        [self.bgImageView addSubview:_btnLogin];
        [_btnLogin mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.inputView.mas_bottom).offset(kHalf(30.0f));
            make.width.mas_equalTo(self.inputView);
            make.height.mas_equalTo(kHalf(88.0f));
            make.centerX.mas_equalTo(self.view.mas_centerX);
        }];
    }
    return _btnLogin;
}

- (void)presentHomePage
{
    CBEBaseTabBarController *tabBar = [[CBEBaseTabBarController alloc] init];
    [[UIApplication sharedApplication] keyWindow].rootViewController = tabBar;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
