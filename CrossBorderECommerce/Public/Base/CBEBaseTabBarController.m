//
//  CBEBaseTabBarController.m
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/7/30.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEBaseTabBarController.h"
#import "CBEBaseNavigationController.h"

@interface CBEBaseTabBarController ()

@end

@implementation CBEBaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIViewController *homeController = [[UIViewController alloc] init];
    CBEBaseNavigationController *homeNav = [[CBEBaseNavigationController alloc] initWithRootViewController:homeController];
    [self addChildViewController:homeNav];
    [self addTitle:@"首页" normalImageName:@"底部导航-首页默认" selectedImageName:@"底部导航-首页选中" viewController:homeController];
    
    UIViewController *humanController = [[UIViewController alloc] init];
    CBEBaseNavigationController *humanNav = [[CBEBaseNavigationController alloc] initWithRootViewController:humanController];
    [self addChildViewController:humanNav];
    [self addTitle:@"人脉" normalImageName:@"底部导航-库存默认" selectedImageName:@"底部导航-库存选中" viewController:humanController];
    
    UIViewController *messageController = [[UIViewController alloc] init];
    CBEBaseNavigationController *messageNav = [[CBEBaseNavigationController alloc] initWithRootViewController:messageController];
    [self addChildViewController:messageNav];
    [self addTitle:@"消息" normalImageName:@"底部导航-统计默认" selectedImageName:@"底部导航-统计选中" viewController:messageController];
    
    UIViewController *newsController = [[UIViewController alloc] init];
    CBEBaseNavigationController *newsNav = [[CBEBaseNavigationController alloc] initWithRootViewController:newsController];
    [self addChildViewController:newsNav];
    [self addTitle:@"资讯" normalImageName:@"底部导航-库存默认" selectedImageName:@"底部导航-库存选中" viewController:newsController];
    
    UIViewController *mineController = [[UIViewController alloc] init];
    CBEBaseNavigationController *mineNav = [[CBEBaseNavigationController alloc] initWithRootViewController:mineController];
    [self addChildViewController:mineNav];
    [self addTitle:@"我的" normalImageName:@"底部导航-我的默认" selectedImageName:@"底部导航-我的选中" viewController:mineController];
    
    // 未选中字体颜色
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor grayColor]} forState:UIControlStateNormal];
    // 选中字体颜色
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor redColor]} forState:UIControlStateSelected];
}

- (void)addTitle:(NSString *)title normalImageName:(NSString *)normalImageName selectedImageName:(NSString *)selectedImageName viewController:(UIViewController *)viewController{
    
    UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:title image:[[UIImage imageNamed:normalImageName]imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)] selectedImage:[[UIImage imageNamed:selectedImageName]imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)]];
    viewController.tabBarItem = tabBarItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
