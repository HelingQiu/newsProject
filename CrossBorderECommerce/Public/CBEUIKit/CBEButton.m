//
//  CBEButton.m
//  CrossBorderECommerce
//
//  Created by Rainer on 2018/8/16.
//  Copyright © 2018年 Rainer. All rights reserved.
//

#import "CBEButton.h"

@interface CBEButton ()
{
    void(^_didClicked)();
}
@end

@implementation CBEButton

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize backgroundColor:(UIColor *)backgroundColor didClicked:(void(^)())didClicked {
    
    self = [super initWithFrame:frame];
    if (self) {
        _didClicked = didClicked;
        
        if (title) {
            [self setTitle:title forState:(UIControlStateNormal)];
        }
        if (titleColor) {
            [self setTitleColor:titleColor forState:(UIControlStateNormal)];
            [self setTitleColor:[titleColor colorWithAlphaComponent:0.5] forState:(UIControlStateHighlighted)];
        }
        if (titleFontSize > 0) {
            self.titleLabel.font = [UIFont systemFontOfSize:titleFontSize];
        }
        if (backgroundColor) {
            self.backgroundColor = backgroundColor;
        }
        [self addTarget:self action:@selector(butonAction) forControlEvents:(UIControlEventTouchUpInside)];
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize backgroundColor:(UIColor *)backgroundColor inView:(UIView *)superView didClicked:(void(^)())didClicked{
    self = [self initWithFrame:frame title:title titleColor:titleColor titleFontSize:titleFontSize backgroundColor:backgroundColor  didClicked:didClicked];
    
    if (self) {
        [superView addSubview:self];
    }
    return self;
}

- (void)butonAction {
    self.selected = !self.selected;
    
    if (_didClicked) {
        _didClicked();
    }
}

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor titleFontSize:(CGFloat)titleFontSize backgroundColor:(UIColor *)backgroundColor backgroundImageName:(NSString *)backgroundImageName highBackgroundImageName:(NSString *)highBackgroundImageName didClicked:(void(^)())didClicked {
    
    self = [self initWithFrame:frame title:title titleColor:titleColor titleFontSize:titleFontSize backgroundColor:backgroundColor didClicked:didClicked];
    if (self) {
        if (backgroundImageName) {
            [self setBackgroundImage:[UIImage imageNamed:backgroundImageName] forState:(UIControlStateNormal)];
        }
        if (highBackgroundImageName) {
            [self setBackgroundImage:[UIImage imageNamed:highBackgroundImageName] forState:(UIControlStateHighlighted)];
        }
    }
    return self;
}

@end
